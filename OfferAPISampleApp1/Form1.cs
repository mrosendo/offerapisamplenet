﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OfferAPISampleApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void submitBtn_Click(object sender, EventArgs e)
        {
            DateTime End_Date = new DateTime(2014, 12, 31);
            DateTime Start_Date = new DateTime(2014, 11, 1);
            String[] Status = { "A" };

            this.output.Text = "Querying the webservice...";
            //API 2.0
            OfferAPISampleApp1.offerApi20.OfferDetail soap20 = new offerApi20.OfferDetailClient();

            //API 2.0 getOffers
            OfferAPISampleApp1.offerApi20.getOffers request20 = new offerApi20.getOffers(this.User_Name.Text, this.Password.Text, Status, null, null, Start_Date, End_Date, null, null, null);
            OfferAPISampleApp1.offerApi20.getOffersResponse response20 = new offerApi20.getOffersResponse();
            try
            {
                response20 = soap20.getOffers(request20);
                this.output.Text = "This is the response for 2.0 \n" + response20.String;
            }
            catch (Exception error)
            {
                this.output.Text = "ERROR in request:" + error + "\n";
            }
            //API 2.0 getOffersType
            OfferAPISampleApp1.offerApi20.getOffersType request20Type = new offerApi20.getOffersType(this.User_Name.Text, this.Password.Text, Status, null, null, Start_Date, End_Date, null, null, null);
            OfferAPISampleApp1.offerApi20.getOffersTypeResponse response20Type = new offerApi20.getOffersTypeResponse();
            try
            {
                response20Type = soap20.getOffersType(request20Type);
                if (response20Type != null)
                {
                    if (response20Type.Offers.Error != null)
                    {
                        this.output20Type.Text = "Response for 2.0 returned ERROR: " + response20Type.Offers.Error.Message + "\n";
                    }
                    else
                    {
                        this.output20Type.Text = "Verified the response for 2.0 was successfull and returned a list of offers.";
                    }
                }
            }
            catch (Exception error)
            {
                this.output20Type.Text = "ERROR in request:" + error + "\n";
            }

            //API 1.3
            OfferAPISampleApp1.offerApi13.OfferDetails soap13 = new offerApi13.OfferDetailsClient();
            //API 1.3 getOffers
            OfferAPISampleApp1.offerApi13.getOffers params13 = new offerApi13.getOffers();
            params13.User_Name = this.User_Name.Text;
            params13.Password = this.Password.Text;
            params13.Status = Status;
            params13.Start_Date = Start_Date;
            params13.End_Date = End_Date;
            OfferAPISampleApp1.offerApi13.getOffersRequest request13 = new offerApi13.getOffersRequest(params13);
            OfferAPISampleApp1.offerApi13.getOffersResponse1 response13 = new offerApi13.getOffersResponse1();
            try
            {
                response13 = soap13.getOffers(request13);
                this.output13.Text = "This is the response for 1.3 \n" + response13.getOffersResponse.String;
            }
            catch (Exception error)
            {
                this.output13.Text = "ERROR in request:" + error + "\n";
            }
            //API 1.3 getOffersType
            OfferAPISampleApp1.offerApi13.getOffersType params13Type = new offerApi13.getOffersType();
            params13Type.User_Name = this.User_Name.Text;
            params13Type.Password = this.Password.Text;
            params13Type.Status = Status;
            params13Type.Start_Date = Start_Date;
            params13Type.End_Date = End_Date;
            OfferAPISampleApp1.offerApi13.getOffersTypeRequest request13Type = new offerApi13.getOffersTypeRequest(params13Type);
            OfferAPISampleApp1.offerApi13.getOffersTypeResponse1 response13Type = new offerApi13.getOffersTypeResponse1();
            try
            {
                response13Type = soap13.getOffersType(request13Type);
                if (response13Type != null)
                {
                    if (response13Type.getOffersTypeResponse.Offers.Error != null)
                    {
                        this.output13Type.Text = "Response for 1.3 returned ERROR: " + response13Type.getOffersTypeResponse.Offers.Error.Message + "\n";
                    }
                    else
                    {
                        this.output13Type.Text = "Verified the response for 1.3 was successfull and returned a list of offers.";
                    }
                }
            }
            catch (Exception error)
            {
                this.output13.Text = "ERROR in request:" + error + "\n";
            }



        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void User_Name_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
